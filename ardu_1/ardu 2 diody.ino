const byte ledPin = 13;
const byte interruptPin = 2;
volatile byte state = LOW;

const int buttonPin2 = 3;
const int ledPin2 = 12;
int buttonStatus2 = 0;

void blink() {
  state = !state;
}

void setup() {
  pinMode(ledPin, OUTPUT);
  pinMode(interruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin), blink, RISING);
  delay(500);

  pinMode(ledPin2, OUTPUT);
  pinMode(buttonPin2, INPUT);
}

void loop() {
  digitalWrite(ledPin, state);

  
  buttonStatus2 = digitalRead(buttonPin2);
  if (buttonStatus2 == HIGH){
    digitalWrite(ledPin2, HIGH);
      }
      else{
        digitalWrite(ledPin2, LOW);
        }
    }
