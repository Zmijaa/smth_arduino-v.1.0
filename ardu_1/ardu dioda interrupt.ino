const int buttonPin = 2;
volatile bool ledState = LOW;

void setup() {
  pinMode(buttonPin, INPUT_PULLUP);
  pinMode(LED_BUILTIN, OUTPUT);
  attachInterrupt(digitalPinToInterrupt(2),
  toggleLedISR, FALLING);
}

void loop() {
  delay(500);

}
void toggleLedISR(){
  ledState = !ledState;
  digitalWrite(LED_BUILTIN, ledState);
}
